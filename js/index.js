var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})

$(function(){
    $('#contacta').on('show.bs.modal', function(e){
      console.log("se esta mostrando el modal");
      $('#contactaBtn').removeClass('btn-outline-warning');
      $('#contactaBtn').addClass('btn-warning');
      $('#contactaBtn').prop('disabled',true);
    });
    $('#contacta').on('shown.bs.modal', function(e){
      console.log("se  mostro el modal");
    });
    $('#contacta').on('hide.bs.modal', function(e){
      console.log("se esta ocultado el modal");  
    });
    $('#contacta').on('hidden.bs.modal', function(e){
      console.log("se oculto el modal");
      $('#contactaBtn').removeClass('btn-warning');
      $('#contactaBtn').addClass('btn-outline-warning');
      $('#contactaBtn').prop('disabled',false);
    });
    $('#contacta').on('hidePrevented.bs.modal', function(e){
      console.log("no puedes cerrar cliclando fuera del modañ");
    });
  });